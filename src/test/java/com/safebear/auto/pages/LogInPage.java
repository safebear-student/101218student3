package com.safebear.auto.pages;

import com.safebear.auto.pages.locators.LogInPageLocators;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;

@RequiredArgsConstructor
public class LogInPage {
    LogInPageLocators locators = new LogInPageLocators();

    @NonNull WebDriver driver;
    private String expectedPageTitle = "Login Page";

    public String getPageTitle() {
        return driver.getTitle();
    }

    public String getExpectedPageTitle() {
        return expectedPageTitle;
    }

    public void login(String un, String pw) {
        driver.findElement(locators.getUsernameLocator()).sendKeys(un);
        driver.findElement(locators.getPasswordLocator()).sendKeys(pw);
        //Submit is the equivalent of hitting enter after entering in creds
        //driver.findElement(locators.getPasswordLocator()).submit();
        driver.findElement(locators.getLoginButtonLocation()).click();
    }

    public void enterUsername(String uname) {
        driver.findElement(locators.getUsernameLocator()).sendKeys(uname);
    }

    public void enterPassword(String pword) {
        driver.findElement(locators.getPasswordLocator()).sendKeys(pword);
    }

    public void rememberMe() {
        if (driver.findElement(locators.getRememberMeLocation()).isSelected()) {
            // Do nothing
        } else {
            driver.findElement(locators.getRememberMeLocation()).click();
        }

    }

    public void clickLogin() {
        driver.findElement(locators.getLoginButtonLocation()).click();
    }

    public String getFailedLoginMessage() {
        return driver.findElement(locators.getUnsuccessfulLoginTextLocation()).getText();
    }
}
