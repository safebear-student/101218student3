package com.safebear.auto.tests;

import com.safebear.auto.pages.LogInPage;
import com.safebear.auto.pages.ToolsPage;
import com.safebear.auto.utils.Utils;
import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class StepDefs {

    WebDriver driver;

    LogInPage logInPage;
    ToolsPage toolsPage;

    @Before
    public void setUp() {
        driver = Utils.getDriver();
        logInPage = new LogInPage(driver);
        toolsPage = new ToolsPage(driver);
    }

    @After
    public void tearDown() {
        try {
            Thread.sleep(Integer.parseInt(System.getProperty("sleep", "2000")));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.quit();
    }

    @Given("^I navigate to the login page$")
    public void i_navigate_to_the_login_page() {
        driver.get(Utils.getUrl());
        Assert.assertEquals(logInPage.getPageTitle(), logInPage.getExpectedPageTitle(), "We're not on the Login Page or its title has changed");
    }

    @When("^I enter the login details for a '(.+)'$")
    public void i_enter_the_login_details_for_a_user(String user) throws Throwable {
        switch (user) {
            case "invalidUser":
                logInPage.enterUsername("attacker");
                logInPage.enterPassword("dontletmein");
                logInPage.clickLogin();
                break;
            case "validUser":
                logInPage.enterUsername("tester");
                logInPage.enterPassword("letmein");
                logInPage.clickLogin();
                break;
            default:
                Assert.fail("The test data is wrong - the only values that can be accepted are 'validUser' or 'invalidUser");
                break;

        }
    }

    @Then("^I can see the following message: '(.+)'$")
    public void i_can_see_the_response_message(String message) throws Throwable {
        switch (message) {
            case "WARNING: Username or Password is incorrect":
                Assert.assertTrue(logInPage.getFailedLoginMessage().contains(message));
                break;
            case "Login Successful":
                Assert.assertTrue(toolsPage.getLoginSuccessMessage().contains(message));
                break;
            default:
                Assert.fail("The test data is wrong");
                break;
        }
    }

}
